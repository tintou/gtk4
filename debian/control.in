Source: gtk4
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Simon McVittie <smcv@debian.org>, @GNOME_TEAM@
Build-Depends: adwaita-icon-theme <!nocheck>,
               at-spi2-core <!nocheck>,
               dbus <!nocheck>,
               debhelper-compat (= 13),
               docbook-xml,
               docbook-xsl,
               dpkg-dev (>= 1.17.14),
               fonts-cantarell <!nocheck>,
               fonts-dejavu-core <!nocheck>,
               gnome-pkg-tools (>= 0.11),
               gobject-introspection (>= 1.41.3),
               gsettings-desktop-schemas <!nocheck>,
               iso-codes <!nocheck>,
               libcairo2-dev (>= 1.14.0),
               libcolord-dev (>= 0.1.9),
               libcups2-dev (>= 2.0),
               libegl1-mesa-dev [linux-any],
               libepoxy-dev,
               libfontconfig1-dev,
               libfribidi-dev (>= 0.19.7),
               libgdk-pixbuf-2.0-dev (>= 2.30.0) | libgdk-pixbuf2.0-dev (>= 2.30.0),
               libgirepository1.0-dev (>= 1.39.0),
               libglib2.0-dev (>= 2.66.0),
               libgraphene-1.0-dev (>= 1.9.1),
               libgraphene-1.0-dev (>= 1.10.4~) [any-i386],
               libharfbuzz-dev (>= 0.9),
               libjson-glib-dev,
               libpango1.0-dev (>= 1.47.0),
               librest-dev,
               librsvg2-common <!nocheck>,
               librsvg2-dev,
               libvulkan-dev [linux-any],
               libwayland-dev (>= 1.14.91) [linux-any],
               libx11-dev,
               libxcomposite-dev,
               libxcursor-dev,
               libxdamage-dev,
               libxext-dev,
               libxfixes-dev,
               libxi-dev,
               libxinerama-dev,
               libxkbcommon-dev (>= 0.2.0),
               libxkbfile-dev,
               libxml2-utils,
               libxrandr-dev (>= 2:1.5.0),
               locales <!nocheck> | locales-all <!nocheck>,
               meson (>= 0.54),
               pkg-config,
               sassc,
               ttf-bitstream-vera <!nocheck>,
               wayland-protocols (>= 1.20) [linux-any],
               xauth <!nocheck>,
               xsltproc,
               xvfb <!nocheck>
Build-Depends-Indep: gtk-doc-tools (>= 1.33),
                     libcairo2-doc,
                     libglib2.0-doc,
                     libpango1.0-doc,
                     pandoc,
Rules-Requires-Root: no
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/gnome-team/gtk4
Vcs-Git: https://salsa.debian.org/gnome-team/gtk4.git
Homepage: https://www.gtk.org/

Package: @SHARED_PKG@
Architecture: any
Multi-Arch: same
Depends: adwaita-icon-theme,
         hicolor-icon-theme,
         shared-mime-info,
         ${misc:Depends},
         ${shlibs:Depends},
         @COMMON_PKG@ (>= ${source:Version})
Provides: @GTK_BINVER_DEP@
Recommends: @BIN_PKG@,
            iso-codes,
            librsvg2-common,
Suggests: gvfs,
Breaks: libgtk-4-0 (<< 4.0.0),
Replaces: libgtk-4-0 (<< 4.0.0),
Pre-Depends: ${misc:Pre-Depends}
Description: GTK graphical user interface library
 GTK is a multi-platform toolkit for creating graphical user
 interfaces. Offering a complete set of widgets, GTK is suitable
 for projects ranging from small one-off tools to complete application
 suites.
 .
 This package contains the shared libraries.

Package: @UDEB_PKG@
Build-Profiles: <!noudeb>
Package-Type: udeb
Section: debian-installer
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Provides: @GTK_BINVER_DEP@
Description: GTK graphical user interface library - minimal runtime
 This is a udeb, or a microdeb, for the debian-installer.
 .
 GTK is a multi-platform toolkit for creating graphical user
 interfaces. Offering a complete set of widgets, GTK is suitable
 for projects ranging from small one-off tools to complete application
 suites.
 .
 This package contains the minimal runtime library using X11 needed
 by the Debian installer.

Package: @COMMON_PKG@
Section: misc
Architecture: all
Multi-Arch: foreign
Depends: libgtk-3-common,
         ${misc:Depends}
Recommends: @SHARED_PKG@
Description: common files for the GTK graphical user interface library
 GTK is a multi-platform toolkit for creating graphical user
 interfaces. Offering a complete set of widgets, GTK is suitable
 for projects ranging from small one-off tools to complete application
 suites.
 .
 This package contains the common files which the libraries need.

Package: @BIN_PKG@
Section: misc
Architecture: any
Multi-Arch: foreign
Depends: gtk-update-icon-cache,
         libgtk-3-bin,
         ${misc:Depends},
         ${shlibs:Depends},
         @COMMON_PKG@ (>= ${source:Version}),
         @SHARED_PKG@ (>= ${source:Version})
Breaks: @EXAMPLES_PKG@ (<< 3.96.0-3~)
Replaces: @EXAMPLES_PKG@ (<< 3.96.0-3~)
Description: programs for the GTK graphical user interface library
 GTK is a multi-platform toolkit for creating graphical user
 interfaces. Offering a complete set of widgets, GTK is suitable
 for projects ranging from small one-off tools to complete application
 suites.
 .
 This package contains the utilities which are used by the libraries
 and other packages.

Package: @DEV_PKG@
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: gir1.2-gtk-4.0 (= ${binary:Version}),
         libcairo2-dev (>= 1.14.0),
         libegl1-mesa-dev [linux-any],
         libepoxy-dev (>= 1.0),
         libfontconfig1-dev,
         libgdk-pixbuf-2.0-dev (>= 2.30.0) | libgdk-pixbuf2.0-dev (>= 2.30.0),
         libglib2.0-dev (>= 2.66.0),
         libgraphene-1.0-dev (>= 1.5.1),
         libpango1.0-dev (>= 1.47.0),
         libwayland-dev (>= 1.9.91) [linux-any],
         libx11-dev,
         libxcomposite-dev,
         libxcursor-dev,
         libxdamage-dev,
         libxext-dev,
         libxfixes-dev,
         libxi-dev,
         libxinerama-dev,
         libxkbcommon-dev,
         libxrandr-dev,
         libvulkan-dev [linux-any],
         pkg-config,
         wayland-protocols (>= 1.9) [linux-any],
         ${misc:Depends},
         ${shlibs:Depends},
         @COMMON_PKG@,
         @SHARED_PKG@ (= ${binary:Version})
Suggests: @DOC_PKG@
Description: development files for the GTK library
 GTK is a multi-platform toolkit for creating graphical user
 interfaces. Offering a complete set of widgets, GTK is suitable
 for projects ranging from small one-off tools to complete application
 suites.
 .
 This package contains the header and development files which are
 needed for building GTK applications.

Package: @DOC_PKG@
Build-Profiles: <!nodoc>
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: libglib2.0-doc,
            libpango1.0-doc
Suggests: devhelp
Description: documentation for the GTK graphical user interface library
 GTK is a multi-platform toolkit for creating graphical user
 interfaces. Offering a complete set of widgets, GTK is suitable
 for projects ranging from small one-off tools to complete application
 suites.
 .
 This package contains the HTML documentation for the GTK library
 in /usr/share/doc/@DOC_PKG@/ .

Package: @EXAMPLES_PKG@
Build-Profiles: <!noinsttest>
Section: x11
Architecture: any
Depends: gtk-3-examples,
         ${misc:Depends},
         ${shlibs:Depends},
         @SHARED_PKG@ (= ${binary:Version})
Recommends: fonts-cantarell,
            librsvg2-common,
Description: example files for GTK 4
 GTK is a multi-platform toolkit for creating graphical user
 interfaces. Offering a complete set of widgets, GTK is suitable
 for projects ranging from small one-off tools to complete application
 suites.
 .
 This package contains the example files and a demonstration program
 for GTK4. It also contains the installed tests.

Package: @TESTS_PKG@
Build-Profiles: <!noinsttest>
Section: x11
Architecture: any
Depends: iso-codes,
         librsvg2-common,
         ${misc:Depends},
         ${shlibs:Depends},
         @SHARED_PKG@ (= ${binary:Version}),
Recommends: gnome-desktop-testing,
            locales | locales-all,
Description: "as-installed" tests for GTK 4
 GTK is a multi-platform toolkit for creating graphical user
 interfaces. Offering a complete set of widgets, GTK is suitable
 for projects ranging from small one-off tools to complete application
 suites.
 .
 This package contains installable test programs, primarily for use with
 autopkgtest.

Package: gir1.2-gtk-4.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends},
         ${misc:Depends},
         ${shlibs:Depends},
         @COMMON_PKG@
Breaks: python-gi (<< 3.18),
        python3-gi (<< 3.18)
Provides: gir1.2-gdk-4.0 (= ${binary:Version}),
          gir1.2-gdkwayland-4.0 (= ${binary:Version}) [linux-any],
          gir1.2-gdkx11-4.0 (= ${binary:Version}),
          gir1.2-gsk-4.0 (= ${binary:Version})
Description: GTK graphical user interface library -- gir bindings
 GTK is a multi-platform toolkit for creating graphical user
 interfaces. Offering a complete set of widgets, GTK is suitable
 for projects ranging from small one-off tools to complete application
 suites.
 .
 This package can be used by other packages using the GIRepository format to
 generate dynamic bindings.

#Package: gtk-update-icon-cache
#Section: misc
#Architecture: any
#Multi-Arch: foreign
#Depends: ${misc:Depends},
#         ${shlibs:Depends}
#Breaks: libgtk2.0-bin (<< 2.24.30-2),
#        libgtk-3-bin (<< 3.20.6-1)
#Replaces: libgtk2.0-bin (<< 2.24.30-2),
#          libgtk-3-bin (<< 3.20.6-1)
#Description: icon theme caching utility
# gtk-update-icon-cache creates mmap()able cache files for icon themes.
# .
# GTK can use the cache files created by gtk-update-icon-cache to avoid a lot
# of system call and disk seek overhead when the application starts. Since the
# format of the cache files allows them to be mmap()ed shared between multiple
# applications, the overall memory consumption is reduced as well.
